'use strict'

const dotenv = require('dotenv')
const express = require('express')
const app = express()
const cors = require('cors')
const path = require('path')
const { moongose } = require('./config/database')

dotenv.config()

// Settings
const port = process.env.PORT || 3000
const app_url = process.env.APP_URL

// Middlewares
app.use(express.json())
app.use(cors())

// Routes
app.use(
    '/api/user',
    require('./routes/user.route')
)

// Starting Serve
app.listen(port, () => {
    console.log(`Server on ${app_url}:${port}`)
})


// DEV MODE
if (process.env.NODE_ENV === 'development') {
    const morgan = require('morgan')
        // Middleware
    app.use(morgan('dev'))
}