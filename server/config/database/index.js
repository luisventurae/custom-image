'use strict'

const dotenv = require('dotenv')
const mongoose = require('mongoose')

dotenv.config()
const uri = process.env.URL_DB

mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(db => console.log('DB connected'))
    .catch(error => console.error(error))

module.exports = mongoose