'use strict'

const mongoose = require('mongoose')
const { Schema } = mongoose

const UserSchema = new Schema({
    name: { type: String, require: true },
    email: { type: String, require: false },
    avatar: { type: String, require: true },
    created: { type: Date, require: true },
})

module.exports = mongoose.model('Users', UserSchema)