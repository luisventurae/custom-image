'use strict'

const mongoose = require('mongoose')
const { Schema } = mongoose

const PictureSchema = new Schema({
    name: { type: String, require: true },
    size: { type: Number, require: true },
    updated: { type: Date, require: true },
})

module.exports = mongoose.model('Pictures', PictureSchema)