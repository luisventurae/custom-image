'use strict'

const express = require('express')
const router = express.Router()
const cors = require('cors')
const userCtrl = require('../controllers/user.controller')

const corsOptions = {
    origin: process.env.URL_CLIENT,
    optionsSuccessStatus: 200
}

router.get('/:id', cors(corsOptions), userCtrl.getUser)
router.post('/', cors(corsOptions), userCtrl.createUser)

module.exports = router