'use strict'

const User = require('../models/User')
const userCtrl = {}

userCtrl.getUser = async(req, res) => {
    try {
        const { id } = req.params
        const user = await User.findById(id)

        return res.json(user)

    } catch (error) {

        return res.json({
            error: true,
            status: 'User does not exist'
        })
    }
}
userCtrl.createUser = async(req, res) => {
    try {
        const user = new User(req.body)
        await user.save()

        return res.json({
            status: 'User saved successfully'
        })

    } catch (error) {
        return res.json({
            error: true,
            status: 'User did not create'
        })
    }

}

module.exports = userCtrl