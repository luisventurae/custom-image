import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'

const firebase = require('firebase/app')
const axios = require('axios')
const url_api = process.env.VUE_APP_URL_API
const headers = {
    'Access-Control-Allow-Origin': '*',
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: '',
        error: ''
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload
        },
        setError(state, payload) {
            state.error = payload
        }
    },
    actions: {
        signup({ commit }, payload) {
            axios.post(`${url_api}/api/user`, {
                    name: "Luis",
                    email: "luis@buzca.pe",
                    avatar: "https://media.canalnet.tv/2018/08/Homero-Simpson.jpeg",
                    created: "Wed Sep 11 2019 20:57:32 GMT-0500"
                })
                .then(result => {
                    firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
                        .then(res => {
                            commit('setUser', { email: res.user.email, uid: res.user.uid })
                            router.push({ name: 'home' })
                        })
                        .catch(err => {
                            commit('setError', err.message)
                        })
                })
                .catch(err => {
                    commit('setError', err.message)
                })
        },
        login({ commit }, payload) {
            firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
                .then(res => {
                    commit('setUser', { email: res.user.email, uid: res.user.uid })
                    router.push({ name: 'home' })
                })
                .catch(err => {
                    commit('setError', err.message)
                })
        },
        getStateLogin({ commit }, payload) {
            if (payload !== null) commit('setUser', { email: payload.email, uid: payload.uid })
            else commit('setUser', null)
        },
        logout({ commit }) {
            firebase.auth().signOut
            commit('setUser', null)
            router.push({ name: 'login' })
        },
    },
    getters: {
        existUser(state) {
            if (state.user === null || state.user === '' || state.user === undefined) return false
            else return true
        }
    },
})