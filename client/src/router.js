import Vue from 'vue'
import Router from 'vue-router'

const firebase = require('firebase/app')

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [{
            path: '*',
            redirect: '/login'
        },
        {
            path: '/',
            name: 'home',
            component: () =>
                import ( /* webpackChunkName: "home" */ './views/Home.vue'),
            meta: { requiresAuth: true }
        },
        {
            path: '/login',
            name: 'login',
            component: () =>
                import ( /* webpackChunkName: "login" */ './views/Login.vue'),
        },
        {
            path: '/signup',
            name: 'signup',
            component: () =>
                import ( /* webpackChunkName: "signup" */ './views/Signup.vue'),
        },
    ]
})

router.beforeEach((to, from, next) => {
    const routeProtected = to.matched.some(record => record.meta.requiresAuth)
    const user = firebase.auth().currentUser
    if (routeProtected === true && user === null) next({ name: 'login' })
    else next()
})

export default router