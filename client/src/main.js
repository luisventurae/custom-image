import Vue from 'vue'
import firebase from 'firebase'
import App from './App.vue'
import router from './router'
import store from './store'

import firebaseConfig from './helpers/firebaseConfig'
import './registerServiceWorker'

firebase.initializeApp(firebaseConfig);
Vue.config.productionTip = false

firebase.auth().onAuthStateChanged(user => {
    if (user) store.dispatch('getStateLogin', { email: user.email, uid: user.uid })
    else store.dispatch('getStateLogin', null)

    new Vue({
        router,
        store,
        render: h => h(App)
    }).$mount('#app')
})